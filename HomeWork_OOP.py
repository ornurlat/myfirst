class Counter():
    value = 0

    def __init__(self, initial_value):
        self.value = initial_value

    def inc(self):
        self.value = self.value + 1
        return self.value

    def dec(self):
        self.value = self.value - 1
        return self.value


class ReverseCounter(Counter):
    def inc(self):
        self.value = self.value - 1
        return self.value

    def dec(self):
        self.value = self.value + 1
        return self.value


def get_counter(number: int):

    if number >= 0:
        return Counter(number)
    return ReverseCounter(number)


counter = get_counter(5)
a = counter.inc()
b = counter.inc()
c = counter.inc()
d = counter.dec()
print(a, b, c, d)

rev_counter = get_counter(-1)
rev_a = rev_counter.inc()
rev_b = rev_counter.inc()
rev_c = rev_counter.inc()
rev_d = rev_counter.dec()
print(rev_a, rev_b, rev_c, rev_d)
# obj_counter1 = Counter(2)
# obj_counter2 = ReverseCounter(2)
