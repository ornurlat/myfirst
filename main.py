"""Задача 1"""


def foo1(day: int, month: int, year: int) -> str:
    months = {1: "Январь", 2: "Февраль", 3: "Март",
              4: "Апрель", 5: "Май",
              6: "Июнь", 7: "Июль", 8: "Август",
              9: "Сентябрь", 10: "Октябрь",
              11: "Ноябрь", 12: "Декабрь"
              }
    return f"{day} {months[month]} {year} года"


# print(foo1(22, 5, 2023))


"""Задача 2"""


def foo2(names: tuple) -> dict:
    name_dict = {}
    for elem in names:
        if elem not in name_dict:
            name_dict[elem] = 1
        else:
            name_dict[elem] += 1
    return name_dict


name_tuple = ('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')
# print(foo2(name_tuple))


"""Задача 3"""


def foo3(person_dict: dict) -> str:
    first_name = person_dict.get('first_name', '')
    last_name = person_dict.get('last_name', '')
    middle_name = person_dict.get('middle_name', '')

    if (not any([first_name, last_name, middle_name])
            or not any([first_name, last_name])):
        return 'Нет данных'
    elif not last_name:
        return f'{first_name} {middle_name}'
    elif not first_name:
        return last_name
    elif not middle_name:
        return f'{last_name} {first_name}'
    else:
        return f'{last_name} {first_name} {middle_name}'


# person = {'first_name': 'Иван', 'last_name': '', 'middle_name': 'Иванович'}
person = {'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': ''}
# person = {'first_name': '', 'last_name': 'Иванов', 'middle_name': 'Иванович'}
# person = {'first_name': '', 'last_name': '', 'middle_name': 'Иванович'}
# person = {'first_name': '', 'last_name': '', 'middle_name': ''}
print(foo3(person))

"""Задача 4"""


def foo4(n) -> bool:
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

# print(foo4(4))


"""Задача 5"""


def foo5(*params) -> list:
    new_list = []
    for i in range(len(params)):
        if isinstance(params[i], int) and params[i] not in new_list:
            new_list.append(params[i])
    new_list = sorted(new_list)
    return new_list


print(foo5(1, '2', 'text', 42, None, None, None, 15, True, 1, 1))